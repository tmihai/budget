import model.Budget;
import model.Category;
import model.Expense;

/**
 * Created by Tiberiu Mihai on 10/23/2014.
 * Email: tiberiu.mihai@oracle.com
 * Phone: 0748 471 512
 */
public class BudgetMain {
    public static void main(String args[]) {
        Budget budget = new Budget();

        budget.addIncome(1000);

        Expense expense = new Expense(15.99f, "12.10.2014", "lunch at KFC", new Category("food"));
        budget.addExpense(expense);

        expense = new Expense(10.65f, "13.10.2014", "phone bill", new Category("bills"));
        budget.addExpense(expense);

        Expense personal = new Expense(5.00f, "14.10.2014", "notebook for school", new Category("personal"));
        budget.addExpense(personal);

        expense = new Expense(34.75f, "15.10.2014", "ski sunglasses", new Category("hobby"));
        budget.addExpense(expense);

        expense = new Expense(35.00f, "16.10.2014", "green ski sunglasses", new Category("hobby"));
        budget.addExpense(expense);

        System.out.println("Balance now:              " + budget.getBalance());
        System.out.println("Expenses average:         " + budget.getExpensesAverage());
        System.out.println("Less expensive category:  " + budget.getLessExpensiveCategory().getType());
        System.out.println("Top expense:              " + budget.getTopExpense().getDescription());
        System.out.println("Top expensive category:   " + budget.getTopExpensiveCategory().getType());

        // See that in the budget only one category exist for "hobby"
        System.out.println("Categories:               " + budget.getCategories().size());

        // See that in "personal" category was removed
        // from the budget because it has no expenses
        // after removing the last expense of it
        budget.removeExpense(personal);
        System.out.println("Categories (remove):      " + budget.getCategories().size());
    }
}

package model;

import java.util.*;

/**
 * Created by Tiberiu Mihai on 10/23/2014.
 * Email: tiberiu.mihai@oracle.com
 * Phone: 0748 471 512
 */
public class Budget {
    private float income;
    private Map<Category, List<Expense>> expenses = new HashMap<Category, List<Expense>>();

    /**
     * Add income to the budget
     *
     * @param income The income value to be added
     */
    public void addIncome(float income) {
        this.income += income;
    }

    /**
     * Remove income from the budget
     *
     * @param income The income value to be decreased
     */
    public void removeIncome(float income) {
        this.income -= income;
    }

    /**
     * Add an expense to the budget
     *
     * @param expense The expense to be added
     */
    public void addExpense(Expense expense) {
        if (!expenses.containsKey(expense.getCategory())) {
            List<Expense> initList = new ArrayList<Expense>();
            initList.add(expense);
            expenses.put(expense.getCategory(), initList);
        } else {
            List<Expense> initList = expenses.get(expense.getCategory());
            initList.add(expense);
            expenses.put(expense.getCategory(), initList);
        }
    }

    /**
     * Remove an expense from the budget
     *
     * @param expense The expense to be removed
     */
    public void removeExpense(Expense expense) {
        if (!expenses.containsKey(expense.getCategory())) {
            return;
        } else {
            List<Expense> initList = expenses.get(expense.getCategory());
            initList.remove(expense);
            if (initList.size() > 0) {
                expenses.put(expense.getCategory(), initList);
            } else {
                expenses.remove(expense.getCategory());
            }
        }
    }

    /**
     * Get the most expensive expense from the budget
     *
     * @return The most expensive expense
     */
    public Expense getTopExpense() {
        Expense topExpense = null;

        Object[] categories = expenses.keySet().toArray();

        if (categories.length > 0) {
            for (Object categoryObj : categories) {
                Category category = (Category) categoryObj;
                List<Expense> categoryExpenses = expenses.get(category);

                int initial = 0;

                if (topExpense == null) {
                    topExpense = categoryExpenses.get(0);
                    initial = 1;
                }

                for (int i = initial; i < categoryExpenses.size(); i++) {
                    Expense expense = categoryExpenses.get(i);
                    if (topExpense.getValue() < expense.getValue()) {
                        topExpense = expense;
                    }
                }
            }
        }

        return topExpense;
    }

    /**
     * Get the budget ballance
     *
     * @return The difference between income and the value of all expenses added in the budget
     */
    public float getBalance() {
        float total = 0f;

        Object[] categories = expenses.keySet().toArray();

        if (categories.length > 0) {
            for (Object categoryObj : categories) {
                Category category = (Category) categoryObj;
                List<Expense> categoryExpenses = expenses.get(category);

                for(Expense expense : categoryExpenses) {
                    total += expense.getValue();
                }
            }
        }

        return this.income - total;
    }

    /**
     * Get the average of all expenses in the budget
     *
     * @return The average value
     */
    public float getExpensesAverage() {
        float total = 0f;
        int countExpenses = 0;

        Object[] categories = expenses.keySet().toArray();

        if (categories.length > 0) {
            for (Object categoryObj : categories) {
                Category category = (Category) categoryObj;
                List<Expense> categoryExpenses = expenses.get(category);

                for(Expense expense : categoryExpenses) {
                    total += expense.getValue();
                }

                countExpenses += categoryExpenses.size();
            }
        }

        return total / countExpenses;
    }

    /**
     * Get the expenses average of the specified category
     *
     * @param category The category for which to calculate the average
     *
     * @return The average value for the specified category
     */
    protected float getExpensesAverage(Category category) {
        float average = 0f;
        List<Expense> categoryExpenses = expenses.get(category);

        for (Expense expense : categoryExpenses) {
            average += expense.getValue();
        }

        return average /= categoryExpenses.size();
    }

    /**
     * Get the less expensive category
     *
     * @return The less expensive category
     */
    public Category getLessExpensiveCategory() {
        Category lessExpensiveCategory = null;
        float lessExpensiveAverage = 0f;

        Object[] categories = expenses.keySet().toArray();

        if (categories.length > 0) {
            if (lessExpensiveCategory == null) {
                Category category = (Category) categories[0];
                lessExpensiveCategory = category;
                lessExpensiveAverage = getExpensesAverage(category);
            }

            for (int i=1; i<categories.length; i++) {
                Category category = (Category) categories[i];
                float average = getExpensesAverage(category);

                if (lessExpensiveAverage > average) {
                    lessExpensiveCategory = category;
                    lessExpensiveAverage = average;
                }
            }
        }

        return lessExpensiveCategory;
    }

    /**
     * Get the most expensive category from the budget
     *
     * @return The most expensive category
     */
    public Category getTopExpensiveCategory() {
        Category topExpensiveCategory = null;
        float topExpensiveAverage = 0f;

        Object[] categories = expenses.keySet().toArray();

        if (categories.length > 0) {
            if (topExpensiveCategory == null) {
                Category category = (Category) categories[0];
                topExpensiveCategory = category;
                topExpensiveAverage = getExpensesAverage(category);
            }

            for (int i=1; i<categories.length; i++) {
                Category category = (Category) categories[i];
                float average = getExpensesAverage(category);

                if (topExpensiveAverage < average) {
                    topExpensiveCategory = category;
                    topExpensiveAverage = average;
                }
            }
        }

        return topExpensiveCategory;
    }

    public Set<Category> getCategories() {
        return expenses.keySet();
    }
}

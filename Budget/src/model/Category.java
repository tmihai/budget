package model;

/**
 * Created by Tiberiu Mihai on 10/23/2014.
 * Email: tiberiu.mihai@oracle.com
 * Phone: 0748 471 512
 */
public class Category {
    private String type;

    public Category(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * This method is overridden to be used by the Map of expenses
     * to identify a category only by it's type attribute value
     *
     * {inherit-doc}
     */
    @Override public int hashCode() {
        final int prime = 31;
        return prime * this.type.hashCode();
    }

    /**
     * This method is overridden to be used by the Map of expenses
     * to identify a category by it's type attribute value
     *
     * {inherit-doc}
     */
    @Override public boolean equals(Object object) {
        if (this == object) {
            return true;
        } else if (!(object instanceof Category)) {
            return false;
        }
        Category category = (Category) object;
        return this.getType().equals(category.getType());
    }
}

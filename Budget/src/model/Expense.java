package model;

import java.util.Date;

/**
 * Created by Tiberiu Mihai on 10/23/2014.
 * Email: tiberiu.mihai@oracle.com
 * Phone: 0748 471 512
 */
public class Expense {
    private float value;
    private String date;
    private String description;
    private Category category;

    public Expense(float value, String date, String description, Category category) {
        this.value = value;
        this.date = date;
        this.description = description;
        this.category = category;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * This method is overridden to be used by the Map of expenses
     * to identify a expense by it's attribute values
     *
     * {inherit-doc}
     */
    @Override public int hashCode() {
        final int prime = 31;
        return prime * this.date.hashCode() *
                       this.description.hashCode() *
                       this.date.hashCode() *
                       this.category.hashCode() *
                       ((Float)this.value).hashCode();
    }

    /**
     * This method is overridden to be used by the Map of expenses
     * to identify a expense by it's attribute values
     *
     * {inherit-doc}
     */
    @Override public boolean equals(Object object) {
        if (this == object) {
            return true;
        } else if (!(object instanceof Expense)) {
            return false;
        }
        Expense expense = (Expense) object;
        return this.getValue() == expense.getValue() &&
               this.getDate().equals(expense.getDate()) &&
               this.getDescription().equals(expense.getDescription()) &&
               this.getCategory().getType().equals(expense.getCategory().getType());
    }
}
